﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Recorder.Core;
using Recorder.Core.Models;
using Recorder.Core.Exceptions;
using CSCore.SoundIn;
using CSC = CSCore;
using CSCore.CoreAudioAPI;
using CSCore.Codecs.WAV;

namespace Recorder.CSCore
{
    public class AudioRecorder : IRecorder
    {
        /** Recorder Core Fields **/
        private IAudioDevice _device;
        private ISavePathService _savePathService;

        /** CSCore Fields **/
        private WasapiCapture _capture = null;
        private WaveWriter _writer = null;

        public AudioRecorder(IAudioDevice device, ISavePathService savePathService)
        {
            _device = device;
            _savePathService = savePathService;
        }

        public void Start()
        {
            if (Status() == RecordingStatus.Recording)
                throw new AlreadyRecordingException();

            _capture = new WasapiCapture();
            _capture.Device = GetDevice(_device);
            if (_capture.Device == null)
                throw new DeviceNotFoundException();
            var savePath = _savePathService.GetPath(_device, "wav");
            _capture.Initialize();
            _writer = new WaveWriter(savePath, _capture.WaveFormat);

            _capture.DataAvailable += DataAvailableHandler;
            _capture.Start();
        }

        private void DataAvailableHandler(object sender, DataAvailableEventArgs e){
            _writer.Write(e.Data, e.Offset, e.ByteCount);
        }

        public MMDevice GetDevice(IAudioDevice device)
        {
            using (MMDeviceEnumerator enumerator = new MMDeviceEnumerator())
            {
                foreach(var d in enumerator.EnumAudioEndpoints(DataFlow.Capture, DeviceState.Active))
                    if (d.DeviceID.ToLower().Contains(device.DeviceId.ToLower()))
                        return d;
            }

            return null;
        }

        public RecordingStatus Status()
        {
            if (_capture == null)
                return RecordingStatus.Ready;
            if (_capture.RecordingState == RecordingState.Recording)
                return RecordingStatus.Recording;
            return RecordingStatus.Ready;
        }

        public void Stop()
        {
            if (_capture == null || Status() != RecordingStatus.Recording)
                throw new NotRecordingException();

            _capture.Stop();
            _writer.Dispose();
            _capture.Dispose();
        }
    }
}
