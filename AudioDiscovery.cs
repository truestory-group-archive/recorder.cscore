﻿using CSCore.CoreAudioAPI;
using Recorder.Core.Interfaces;
using Recorder.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.CSCore
{
    public class AudioDiscovery : IAudioDiscovery
    {
        public IEnumerable<IAudioDevice> GetDevices()
        {
            var devices = new List<IAudioDevice>();
            using (MMDeviceEnumerator enumerator = new MMDeviceEnumerator())
            {
                foreach (var d in enumerator.EnumAudioEndpoints(DataFlow.Capture, DeviceState.Active))
                {
                    devices.Add(new AudioDevice
                    {
                        Name = d.FriendlyName,
                        DeviceId = d.DeviceID,
                        PNPDeviceId = d.DeviceID
                    });
                }
            }

            return devices;
        }
    }
}
