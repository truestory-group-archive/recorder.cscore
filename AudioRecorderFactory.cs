﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recorder.CSCore
{
    public class AudioRecorderFactory : IAudioRecorderFactory
    {
        private ISavePathService _savePathService;

        public AudioRecorderFactory(ISavePathService savePathService)
        {
            _savePathService = savePathService;
        }
        public IRecorder Create(IAudioDevice device)
        {
            return new AudioRecorder(device, _savePathService);
        }
    }
}
